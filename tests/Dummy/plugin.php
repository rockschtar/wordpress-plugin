<?php
/*
  Plugin Name: Clubfans United - Editorial
  Plugin URI: http://www.clubfans-united.de
  Description: Spieler, Pressespiegel, Kurzmeldungen, Spenden, Shortcodes, Saison & Widgets
  Author: Stefan Helmer
  Version: develop
  Author URI: http://eracer.de
 */

use Rockschtar\WordPress\Plugin\Tests\PluginDummy;

PluginDummy::run();