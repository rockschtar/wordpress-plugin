<?php


namespace Rockschtar\WordPress\Plugin\Tests;


use Rockschtar\WordPress\Plugin\Controller;

class ControllerDummy extends Controller {

    public function addHooks(): void {
        $this->addAction('init', 'initHook');
    }

    private function initHook(): void {
        $a = 1;
    }
}