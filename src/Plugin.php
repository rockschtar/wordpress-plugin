<?php


namespace Rockschtar\WordPress\Plugin;

abstract class Plugin {

    /**
     * @var Controller[]
     */
    protected $controllers = [];

    abstract public function init();

    private function __construct() {
        $this->init();

        foreach($this->controllers as $subscriber) {
            $subscriber->addHooks();
        }
    }

    public function addController(Controller $subscriber) : void {
        $this->controllers[] = $subscriber;
    }

    final public static function run() : void {

        static $instance = null;
        /** @noinspection ClassConstantCanBeUsedInspection */
        $class = get_called_class();
        if ($instance === null) {
            $instance = new $class();
        }
        return $instance;
    }

}