<?php


namespace Rockschtar\WordPress\Plugin;


abstract class Controller {

    use HookTrait;

    abstract public function addHooks() : void;
}